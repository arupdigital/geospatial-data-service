const express = require('express');
const app = express();
const cors = require('cors');
const apiRouter = require('./routes/api');
const cluster = require('cluster');
const {PORT = 9090} = process.env;

if (cluster.isMaster) {
  const cpuCount = require('os').cpus().length;

  for (let i = 0; i < cpuCount; i += 1) {
    cluster.fork();
  }

  cluster.on('exit', function (worker) {
    console.log('Worker %d died :(', worker.id);
    cluster.fork();
  });

} else {

  app.use(cors());
  app.use(express.json({limit: '50mb'}));
  app.use(express.static('public'));

  app.use('/api', apiRouter);
  app.get('/', (req, res) => res.redirect('/api'));

  app.use('/*', (req, res, next) => {
    next({status:404});
  });

  app.use((err, req, res, next) => {
    console.log(err);
    if (err.status === 400) res.status(400).send({msg : 'Bad request'});
    else if (err.status === 403) res.status(403).send({msg: 'Access denied'});
    else if (err.status === 404) res.status(404).send({msg : 'Page not found'});
    else res.status(500).send({msg : 'Internal server error'});
    next();
  });

  const server = app.listen(PORT, (err) => {
    if (err) throw err;
    console.log(`Worker ${cluster.worker.id} listening on port ${PORT}`);
  });

  server.setTimeout(600000);
}

module.exports = app;