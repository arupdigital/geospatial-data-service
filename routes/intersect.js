const router = require('express').Router();
const getIntersections = require('../controllers/intersect');

router.post('/', getIntersections);

module.exports = router;