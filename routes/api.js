const router = require('express').Router();
const getApiDocs = require('../controllers/api');
const intersectRouter = require('./intersect');
const zipRouter = require('./zip');

router.get('/', getApiDocs);
router.use('/intersect', intersectRouter);
router.use('/zip', zipRouter);

module.exports = router;