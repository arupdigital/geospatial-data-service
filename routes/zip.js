const router = require('express').Router();
const getZip = require('../controllers/zip');

router.post('/', getZip);

module.exports = router;