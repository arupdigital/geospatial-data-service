const turf = require('@turf/turf');

function getIntersections (req, res) {
  const {boundary, sites} = req.body;
  console.log('Finding intersections...');
  console.log(`Job started at ${new Date().toLocaleString()}`);
  const intersectingSites = sites.filter(site => {
    if (site.geometry.type === 'MultiPolygon') {
      return site.geometry.coordinates.some(coords => {
        return booleanIntersects(boundary, turf.polygon([...coords]));
      });
    } 
    else return booleanIntersects(boundary, site);
  });
  console.log('Intersections have been calculated!');
  console.log(`Job completed at ${new Date().toLocaleString()}`);
  res.status(200)
    .send(intersectingSites);
}

function booleanIntersects (boundary, site) {
  return turf.booleanContains(boundary, site) || turf.booleanOverlap(boundary, site) || turf.booleanContains(site, boundary);
}

module.exports = getIntersections;