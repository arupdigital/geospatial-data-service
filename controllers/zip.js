const fs = require('fs');
const archiver = require('archiver');

const getZip = (req, res, next) => {
  const {files} = req.body;
  const path = `./zips/${new Date().toISOString()}.zip`;
  const output = fs.createWriteStream(path);
  const zip = archiver('zip');
  zip.on('warning', console.warn);
  zip.on('error', next);
  zip.on('finish', () => fs.createReadStream(path).pipe(res));
  zip.pipe(output);
  files.forEach(file => {
    const {data, filename, extension} = file;
    const decoded = Buffer.from(data, 'base64');
    zip.append(decoded, {name: `${filename}.${extension}`});
  });
  zip.finalize();
};

module.exports = getZip;